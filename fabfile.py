# -*- coding: utf-8 -*-
from __future__ import with_statement
from fabric.api import *
from fabric.contrib.files import exists, append, contains
from os import path
import os, json, datetime
from time import sleep
import sys

djangoplus_repository_url = 'git@bitbucket.org:brenokcc/djangoplus.git'
digital_ocean_token = '9272901b19cf5b36198cbf9aa9600aa414cf43bff754dc01a4ef7d3291957e53'
hostname = 'probicho.tk'
username = 'root'
bitbucket_username = 'adelsonllima'
bitbucket_password = 'batata_10'
project_dir = os.getcwd()
project_name = project_dir.split('/')[-1]
remote_project_dir = '/var/opt/%s' % project_name
domain_name = hostname


GIT_INGORE_FILE_CONTENT = '''*.pyc
.svn
.DS_Store
.DS_Store?
._*
.idea/*
.Spotlight-V100
.Trashes
ehthumbs.db
Thumbs.db
.project
.pydevproject
.settings/*
sqlite.db
media/*
mail/*
'''
NGINEX_FILE_CONTENT = '''server {
    client_max_body_size 100M;
    listen 80;
    server_name %(domain_name)s;
    access_log /var/opt/%(project_name)s/logs/nginx_access.log;
    error_log /var/opt/%(project_name)s/logs/nginx_error.log;
    location /static {
        alias /var/opt/%(project_name)s/static;
    }
    location /media {
        alias /var/opt/%(project_name)s/media;
    }
    location / {
        proxy_pass_header Server;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_set_header X-Real_IP $remote_addr;
        proxy_set_header X-Scheme $scheme;
        proxy_connect_timeout 600s;
        proxy_send_timeout 600s;
        proxy_read_timeout 600s;
        proxy_pass http://localhost:%(port)s/;
    }
}
'''
SUPERVISOR_FILE_CONTENT = '''[program:%(project_name)s]
directory = /var/opt/%(project_name)s
user = www-data
command = /var/opt/%(project_name)s/gunicorn_start.sh
stdout_logfile = /var/opt/%(project_name)s/logs/supervisor_out.log
stderr_logfile = /var/opt/%(project_name)s/logs/supervisor_err.log
'''
GUNICORN_FILE_CONTENT = '''#!/bin/bash
set -e
source /var/opt/.virtualenvs/%(project_name)s/bin/activate
mkdir -p /var/opt/%(project_name)s/logs
cd /var/opt/%(project_name)s
exec gunicorn %(project_name)s.wsgi:application -w 1 -b 127.0.0.1:%(port)s --timeout=600 --user=www-data --group=www-data --log-level=debug --log-file=/var/opt/%(project_name)s/logs/gunicorn.log 2>>/var/opt/%(project_name)s/logs/gunicorn.log
'''
LIMITS_FILE_CONTENT = '''
*               soft     nofile           65536
*               hard     nofile           65536
root            soft     nofile           65536
root            hard     nofile           65536
'''
BASHRC_FILE_CONTENT = '''
export WORKON_HOME=/var/opt/.virtualenvs
mkdir -p $WORKON_HOME
source /usr/local/bin/virtualenvwrapper.sh
'''


def debug(s):
    print '\n\n\n\n\n[%s] %s' % (datetime.datetime.now(), s)


def available_port():
    nginex_dir = '/etc/nginx/sites-enabled'
    port = 8000
    with cd(nginex_dir):
        files = run('ls').split()
        files.remove('default')
        if project_name in files:
            files = [project_name]
        if files:
            command = "grep  localhost %s | grep -o '[0-9]*'" % ' '.join(files)
            ports = run(command).split()
            ports.sort()
            port = ports[-1]
            if project_name not in files:
                port = int(port) + 1
    debug('Returning port %s!' % port)
    return port


def check_local_keys():
    local_home_dir = local('echo $HOME', capture=True)
    local_ssh_dir = path.join(local_home_dir, '.ssh')
    local_public_key_path = path.join(local_ssh_dir, 'id_rsa.pub')
    if not os.path.exists(local_ssh_dir):
        debug('Creating dir %s...' % local_ssh_dir)
        local('mkdir %s' % local_ssh_dir)
        if not path.exists(local_public_key_path):
            local("ssh-keygen -f %s/id_rsa -t rsa -N ''" % local_ssh_dir)

    key = open(local_public_key_path, 'r').read().strip()
    debug('Checking if private key was uploaded to bitbucket...')
    params = (bitbucket_username, bitbucket_password, bitbucket_username)
    command = 'curl -X GET -u "%s:%s" https://api.bitbucket.org/1.0/users/%s/ssh-keys' % params
    response = local(command, capture=True)

    if key not in response:
        debug('Uploading private key to bitbucket...')
        params = (bitbucket_username, bitbucket_password, bitbucket_username, key)
        command = 'curl -X POST -u "%s:%s" https://api.bitbucket.org/1.0/users/%s/ssh-keys --data-urlencode "key=%s"' % params
        response = local(command, capture=True)
        # print response

    debug('Checking if private key was uploaded to digital ocean...')
    url = 'https://api.digitalocean.com/v2/account/keys'
    params = (digital_ocean_token, url)
    command = '''curl -X GET -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s"''' % params
    response = local(command, capture=True)
    # print response
    if key not in response:
        debug('Uploading private key to digital ocean...')
        params = (digital_ocean_token, 'Default', key, url)
        command = '''curl -X POST -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' -d '{"name":"%s","public_key":"%s"}' "%s"''' % params
        response = local(command, capture=True)
        # print response


def check_remote_keys():
    local_home_dir = local('echo $HOME', capture=True)
    local_ssh_dir = path.join(local_home_dir, '.ssh')
    local_public_key_path = path.join(local_ssh_dir, 'id_rsa.pub')
    local_private_key_path = path.join(local_ssh_dir, 'id_rsa')

    remote_home_dir = run('echo $HOME')
    remote_ssh_dir = path.join(remote_home_dir, '.ssh')
    remote_public_key_path = path.join(remote_ssh_dir, 'id_rsa.pub')
    remote_private_key_path = path.join(remote_ssh_dir, 'id_rsa')
    remote_private_known_hosts_path = path.join(remote_ssh_dir, 'known_hosts')
    if not exists(remote_ssh_dir):
        debug('Creading remote dir %s...' % remote_ssh_dir)
        run('mkdir -p %s' % remote_ssh_dir)
        debug('Creating empty file %s...' % remote_private_known_hosts_path)
        run('touch %s' % remote_private_known_hosts_path)

    with cd(remote_ssh_dir):
        public_key = open(local_public_key_path, 'r').read()
        private_key = open(local_private_key_path, 'r').read()
        debug('Checking if public key is in file %s...' % remote_public_key_path)
        if not contains(remote_public_key_path, public_key):
            debug('Appending public key in file %s...' % remote_public_key_path)
            append(remote_public_key_path, public_key)
        debug('Checking if private key is in file %s...' % remote_private_key_path)
        if not contains(remote_private_key_path, private_key):
            debug('Appending private key in file %s...' % remote_private_key_path)
            append(remote_private_key_path, private_key)
        run('chmod 644 %s' % remote_public_key_path)
        run('chmod 600 %s' % remote_private_key_path)
        debug('Checking if bitbucket.org is in file %s...' % remote_private_known_hosts_path)
        if not contains(remote_private_known_hosts_path, 'bitbucket.org'):
            debug('Appending bitbucket.org in file %s...' % remote_private_known_hosts_path)
            run('ssh-keyscan bitbucket.org >> %s' % remote_private_known_hosts_path)


def check_repository():
    debug('Checking if the project was uploaded to bitbucket...')
    url = 'https://bitbucket.org/api/2.0/repositories'
    params = (bitbucket_username, bitbucket_password, url, bitbucket_username, project_name)
    command = 'curl -X GET -u "%s:%s" %s/%s/%s' % params
    data = json.loads(local(command, capture=True))
    if 'error' in data:
        debug('Uploading project to bitbucket...')
        params = (bitbucket_username, bitbucket_password, url, bitbucket_username, project_name, project_name)
        command = 'curl -X POST -u "%s:%s" %s/%s/%s  -d \'{"name":"%s"}\'' % params
        data = json.loads(local(command, capture=True))
    repository_url = data['links']['clone'][1]['href']
    debug('Project uploaded to bitbucket! URL: %s' % repository_url)
    return repository_url


def setup_local_repository():
    debug('Checking if local project is a git project...')
    if not path.exists(path.join(project_dir, '.git')):
        with cd(project_dir):
            debug('Making local project a git project...')
            repository_url = check_repository()
            local('git init')
            local('git remote add origin "%s"' % repository_url)
            local('echo "..." > README.md')
            local('echo "%s" > .gitignore' % GIT_INGORE_FILE_CONTENT)


def setup_remote_repository():
    debug('Checking if the project was cloned in remote server...')
    if not exists(remote_project_dir):
        with cd('/var/opt'):
            debug('Cloning project in remote server...')
            repository_url = check_repository()
            run('git clone %s %s' % (repository_url, project_name))
            run('chown -R www-data.www-data %s' % project_name)
    debug('Updating project in remote server...')
    with cd(remote_project_dir):
        run('git pull origin master')


def push_local_changes():
    debug('Checking if project has local changes...')
    now = datetime.datetime.now().strftime("%Y%m%d %H:%M:%S")
    with cd(project_dir):
        if 'nothing to commit' not in local('git status', capture=True):
            debug('Comminting local changes...')
            files = []
            for file_name in local('ls', capture=True).split():
                if file_name not in GIT_INGORE_FILE_CONTENT and not file_name.endswith('.pyc'):
                    files.append(file_name)
            files.append('.gitignore')
            for pattern in NGINEX_FILE_CONTENT.split():
                if pattern in files: files.remove(pattern)
            local('git add %s' % ' '.join(files))
            local("git commit -m '%s'" % now)
        debug('Uploading local changes...')
        local('git push origin master')


def setup_remote_env():
    debug('Checking if the virtualenv dir was created in remote server...')
    virtual_env_dir = '/var/opt/.virtualenvs'
    if not exists(virtual_env_dir):
        debug('Creating dir %s' % virtual_env_dir)
        run('mkdir -p %s' % virtual_env_dir)
    project_env_dir = path.join(virtual_env_dir, project_name)
    djangoplus_dir = path.join(project_env_dir, 'lib/python2.7/site-packages/djangoplus')
    debug('Checking if virtualenv for the project was created...')
    if not exists(project_env_dir):
        with shell_env(WORKON_HOME=virtual_env_dir):
            debug('Creating virtual env %s' % project_name)
            run('source /usr/local/bin/virtualenvwrapper.sh && mkvirtualenv %s' % project_name)
            run('git clone %s %s' % (djangoplus_repository_url, djangoplus_dir))
    with cd(djangoplus_dir):
        debug('Updating djangoplus project virtual env...')
        run('git pull origin master')
        run("find . -name '*.pyc' -delete")


def setup_remote_project():
    with cd(remote_project_dir):
        debug('Checking project requirements..')
        if exists('requirements.txt'):
            virtual_env_dir = '/var/opt/.virtualenvs'
            with shell_env(WORKON_HOME=virtual_env_dir):
                debug('Installing/Updating project requirements...')
                run('source /usr/local/bin/virtualenvwrapper.sh && workon %s && pip install -U -r requirements.txt' % project_name)
        debug('Checking if necessary dirs (logs, media and static) were created...')
        run('mkdir -p logs')
        run('mkdir -p static')
        run('mkdir -p media')
        debug('Granting access to www-data...')
        run('chown -R www-data.www-data .')


def check_domain():
    url = 'https://api.digitalocean.com/v2/domains'
    params = (digital_ocean_token, url, domain_name)
    command = '''curl -X GET -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s/%s"''' % params
    debug('Checking if domain %s was created...' % domain_name)
    data = json.loads(local(command, capture=True))
    print data
    if data.get('id', None) == 'not_found':
        debug('Creating domain %s...' % domain_name)
        ip_address = env.hosts[0]
        params = (digital_ocean_token, domain_name, ip_address, url)
        command = '''curl -X POST -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' -d '{"name":"%s","ip_address":"%s"}' "%s"''' % params
        data = json.loads(local(command, capture=True))


def setup_nginx_file():
    file_path = '/etc/nginx/sites-enabled/%s ' % project_name
    debug('Checking nginx file %s...' % file_path)
    if not exists(file_path):
        debug('Creating nginx file %s...' % file_path)
        port = available_port()
        data = dict(project_name=project_name, domain_name=domain_name, port=port)
        text = NGINEX_FILE_CONTENT % data
        append(file_path, text)
        debug('Restarting nginx...')
        run('/etc/init.d/nginx restart')


def setup_supervisor_file():
    file_path = '/etc/supervisor/conf.d/%s.conf ' % project_name
    debug('Checking supervisor file %s...' % file_path)
    if not exists(file_path):
        debug('Creating supervisor file %s...' % file_path)
        data = dict(project_name=project_name)
        text = SUPERVISOR_FILE_CONTENT % data
        append(file_path, text)
        debug('Reloading supervisorctl...')
        run('supervisorctl reload')


def setup_gunicorn_file():
    file_path = '/var/opt/%s/gunicorn_start.sh ' % project_name
    debug('Checking gunicorn file %s...' % file_path)
    if not exists(file_path):
        debug('Creating gunicorn file %s' % file_path)
        port = available_port()
        data = dict(project_name=project_name, port=port)
        text = GUNICORN_FILE_CONTENT % data
        append(file_path, text)
        run('chmod a+x %s' % file_path)


def setup_remote_webserver():
    setup_nginx_file()
    setup_supervisor_file()
    setup_gunicorn_file()


def reload_remote_application():
    debug('Updating project in remote server...')
    with cd(remote_project_dir):
        virtual_env_dir = '/var/opt/.virtualenvs'
        with shell_env(WORKON_HOME=virtual_env_dir):
            run('source /usr/local/bin/virtualenvwrapper.sh && workon %s && python manage.py sync' % project_name)
            run('chown -R www-data.www-data .')
            run('chmod a+w *.db')
            run('ls -l')
            debug('Restarting supervisorctl...')
            run('supervisorctl restart %s' % project_name)


def delete_remote_project():
    debug('Deleting remove project...')
    run('rm -r %s' % remote_project_dir)


def delete_remote_env():
    debug('Deleting remote env...')
    run('rmvirtualenv %s' % project_name)


def delete_domain():
    url = 'https://api.digitalocean.com/v2/domains'
    debug('Deleting domain %s...' % domain_name)
    params = (digital_ocean_token, url, domain_name)
    command = '''curl -X DELETE -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s/%s"''' % params
    local(command)


def delete_repository():
    debug('Deleting project from bitbucket repository...')
    url = 'https://bitbucket.org/api/2.0/repositories'
    params = (bitbucket_username, bitbucket_password, url, bitbucket_username, project_name)
    command = '''curl -X DELETE -u "%s:%s" %s/%s/%s''' % params
    local(command)


def delete_local_repository():
    debug('Deleting local repository...')
    with cd(project_dir):
        local('rm -rf .git')


def delete_nginx_file():
    debug('Deleting nginx file...')
    file_path = '/etc/nginx/sites-enabled/%s ' % project_name
    run('rm %s' % file_path)


def delete_supervisor_file():
    debug('Deleting supervisor file..')
    file_path = '/etc/supervisor/conf.d/%s.conf ' % project_name
    run('rm %s' % file_path)


def reload_remote_webserver():
    debug('Reloading supervisorctl...')
    run('supervisorctl reload')
    debug('Reloading nginx...')
    run('/etc/init.d/nginx restart')
    debug('Starting supervisor...')
    run('service supervisor start')


def configure_crontab():
    debug('Configuring crontab...')
    output = run("crontab -l")
    line = '0 * * * * /var/opt/.virtualenvs/%s/bin/python /var/opt/%s/manage.py backup >/tmp/cron.log 2>&1' % (project_name, project_name)
    if line not in output:
        run('crontab -l | { cat; echo "%s"; } | crontab -' % line)


def check_droplet():

    check_local_keys()

    droplet_id = None
    url = 'https://api.digitalocean.com/v2/droplets/'
    params = (digital_ocean_token, url)
    command = '''curl -X GET -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s"''' % params
    debug('Checking if droplet exists...')
    response = json.loads(local(command, capture=True))

    for droplet in response[u'droplets']:
        if droplet['name'] == hostname:
            droplet_id = droplet['id']
            break

    if droplet_id:
        ip_address = droplet['networks']['v4'][0]['ip_address']
        debug('Droplet found with IP %s' % ip_address)
        local_home_dir = local('echo $HOME', capture=True)
        local_known_hosts_path = path.join(local_home_dir, '.ssh/known_hosts')
        debug('Checking if file %s exists...' % local_known_hosts_path)
        if not os.path.exists(local_known_hosts_path):
            debug('Creating empty file %s...' % local_known_hosts_path)
            local('touch %s' % local_known_hosts_path)
        local_known_hosts_file_content = open(local_known_hosts_path, 'r').read()
        if ip_address not in local_known_hosts_file_content:
            debug('Registering %s as known host...' % ip_address)
            sleep(5)
            local('ssh-keyscan -T 15 %s >> %s' % (ip_address, local_known_hosts_path))
        if 'bitbucket.org' not in local_known_hosts_file_content:
            debug('Registering bitbucket.org as known host...')
            local('ssh-keyscan bitbucket.org >> %s' % local_known_hosts_path)
        return ip_address
    else:
        raise Exception(u'There is no droplet with name %s at Digital Ocean for this accout. '
                        u'Please run the command "create_droplet" to create one.' % hostname)


def delete_droplet():
    url = 'https://api.digitalocean.com/v2/droplets/'
    params = (digital_ocean_token, url)
    command = '''curl -X GET -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s"''' % params
    debug('Checking if droplet exists...')
    response = json.loads(local(command, capture=True))

    for droplet in response[u'droplets']:
        if droplet['name'] == hostname:
            droplet_id = droplet['id']
            url = 'https://api.digitalocean.com/v2/droplets/%s' % droplet_id
            params = (digital_ocean_token, url)
            debug('Deleting droplet...' % droplet)
            command = '''curl -X DELETE -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s"''' % params
            #response = json.loads(local(command, capture=True))
            #print response
            break


def create_droplet():
    url = 'https://api.digitalocean.com/v2/account/keys'
    params = (digital_ocean_token, url)
    debug('Getting installed keys at digital ocean...')
    command = '''curl -X GET -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s"''' % params
    response = json.loads(local(command, capture=True))
    # print response
    ssh_keys = []
    for ssh_key in response['ssh_keys']:
        ssh_keys.append(ssh_key['id'])

    debug('Creating droplet...')
    url = 'https://api.digitalocean.com/v2/droplets/'
    params = digital_ocean_token, hostname, 'nyc3', '512mb', 'debian-8-x64', ssh_keys, url
    command = '''curl -X POST -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' -d '{"name":"%s","region":"%s","size":"%s","image":"%s", "ssh_keys":%s}' "%s"''' % params
    response = json.loads(local(command, capture=True))
    # print response
    droplet_id = response['droplet']['id']

    url = 'https://api.digitalocean.com/v2/droplets/%s/' % droplet_id
    params = (digital_ocean_token, url)
    command = '''curl -X GET -H 'Content-Type: application/json' -H 'Authorization: Bearer %s' "%s"''' % params
    response = json.loads(local(command, capture=True))
    # print response
    ip_address = response['droplet']['networks']['v4'][0]['ip_address']
    debug('Droplet created with IP %s!' % ip_address)
    return check_droplet()


def execute_aptget():
    with cd('/'):
        run('apt-get update')
        run('apt-get -y install build-essential python-dev python-pip git nginx supervisor libncurses5-dev')
        run('apt-get -y install vim')
        run('apt-get -y install libjpeg62-turbo-dev libopenjpeg-dev libfreetype6-dev libtiff5-dev liblcms2-dev libwebp-dev tk8.6-dev libjpeg-dev')
        run('apt-get -y install htop')

        if not contains('/etc/security/limits.conf', '65536'):
            # print LIMITS_FILE_CONTENT
            append('/etc/security/limits.conf', LIMITS_FILE_CONTENT)

        run('pip install virtualenv virtualenvwrapper')

        if not contains('/root/.bashrc', 'WORKON_HOME'):
            # print BASHRC_FILE_CONTENT
            append('/root/.bashrc', BASHRC_FILE_CONTENT)

        if not exists('/swap.img'):
            run('lsb_release -a')
            run('dd if=/dev/zero of=/swap.img bs=1024k count=2000')
            run('mkswap /swap.img')
            run('swapon /swap.img')
            run('echo "/swap.img    none    swap    sw    0    0" >> /etc/fstab')


def backupdb():
    local_home_dir = local('echo $HOME', capture=True)
    backup_dir = os.path.join(local_home_dir, 'backup')
    if not os.path.exists(backup_dir):
        local('mkdir -p %s' % backup_dir)
    with cd('/var/opt'):
        for entry in run('ls').split():
            file_name = '/var/opt/%s/sqlite.db' % entry
            bakcup_file_name = os.path.join(backup_dir, '%s.db' % entry)
            if exists(file_name):
                command = 'scp %s@%s:%s %s' % (username, hostname, file_name, bakcup_file_name)
                local(command)


def undeploy():
    delete_remote_project()
    delete_remote_env()
    delete_domain()
    delete_repository()
    delete_local_repository()
    delete_nginx_file()
    delete_supervisor_file()
    reload_remote_webserver()


def deploy():

    #with hide('running','warnings'):#, 'output'
        check_domain()
        check_remote_keys()
        execute_aptget()
        setup_local_repository()
        push_local_changes()
        setup_remote_env()
        setup_remote_repository()
        setup_remote_project()
        setup_remote_webserver()
        reload_remote_application()


def update():
    push_local_changes()
    setup_remote_env()
    setup_remote_repository()
    setup_remote_project()
    reload_remote_application()


def push():
    push_local_changes()
    setup_remote_env()
    setup_remote_repository()
    reload_remote_application()

if '-l' in sys.argv:
    env.hosts = []
else:
    env.hosts = [check_droplet()]
env.user = username
env.connection_attempts = 10