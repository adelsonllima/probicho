# -*- coding: utf-8 -*-
from djangoplus.db import models
from djangoplus.decorators import attr, action, subset


class Estado(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)
    sigla = models.CharField(verbose_name=u'Sigla', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', 'sigla')}),
        (u'Cadastros::Municípios', {'relations': ('municipio_set',),}),
    )

    class Meta:
        ordering = ['nome']
        verbose_name = u'Estado'
        verbose_name_plural = u'Estados'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return self.nome


class Municipio(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)
    estado = models.ForeignKey('Estado', verbose_name=u'Estado')
    # Quando se define um related_name dá erro
    # estado = models.ForeignKey('Estado', verbose_name=u'Estado', related_name='municipios')

    class Meta:
        ordering = ['nome']
        verbose_name = u'Município'
        verbose_name_plural = u'Municípios'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return u'%s/%s' % (self.nome, self.estado)


class PessoaFisica(models.Model):
    SEXO_MASCULINO = 'M'
    SEXO_FEMININO = 'F'
    SEXO_CHOICES = (
        (SEXO_MASCULINO, u'Masculino'),
        (SEXO_FEMININO, u'Feminino'),
    )

    # usuario = models.OneToOneField('Usuario', null=True, blank=True)
    nome = models.CharField(verbose_name=u'Nome', search=True)
    cpf = models.CpfField(verbose_name=u'CPF', search=True)
    sexo = models.CharField(verbose_name=u'Sexo', max_length=1, choices=SEXO_CHOICES, null=True, blank=True, search=True)
    telefone = models.PhoneField('Telefone', blank=True, default='')
    email = models.CharField(verbose_name=u'E-mail', blank=True, default='')

    #endereco
    logradouro = models.CharField(verbose_name=u'Logradouro', blank=True)
    numero = models.CharField(verbose_name=u'Número', blank=True)
    complemento = models.CharField(verbose_name=u'Complemento', blank=True)
    bairro = models.CharField(verbose_name=u'Bairro', blank=True)
    cep = models.CharField(verbose_name=u'CEP', blank=True)
    munipio = models.ForeignKey(Municipio, verbose_name=u'Município', null=True, blank=True)
    ponto_referencia = models.TextField(verbose_name=u'Ponto de referência', blank=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', ('cpf', 'sexo'), ('email', 'telefone'),)}),
        (u'Endereço', {'fields': (('logradouro', 'numero'), ('bairro', 'cep'), ('complemento', 'municipio'), 'ponto_referencia')}),
    )

    class Meta:
        ordering = ['nome']
        verbose_name = u'Pessoa Física'
        verbose_name_plural = u'Pessoas Físicas'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return self.nome


class Porte(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', )}),
        (u'Estatísica::Gráficos', {'extra': ('get_quantidade',)}),
    )

    class Meta:
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return self.nome

    @attr(u'Quantidade de Animais', template_filter='chart')
    def get_quantidade(self):
        return Animal.objects.filter(porte=self).count('porte')


class Especie(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', )}),
        (u'Cadastros::Raças', {'relations': ('raca_set',),}),
        (u'Estatísica::Gráficos', {'extra': ('get_quantidade',)}),
    )

    class Meta:
        ordering = ['nome']
        verbose_name = u'Espécie'
        verbose_name_plural = u'Espécies'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return self.nome

    @attr(u'Quantidade de Animais', template_filter='chart')
    def get_quantidade(self):
        return Animal.objects.filter(raca__especie=self).count('raca')


class Raca(models.Model):
    nome = models.CharField(verbose_name=u'Nome', search=True)
    especie = models.ForeignKey('Especie', verbose_name=u'Espécie')

    class Meta:
        ordering = ['nome']
        verbose_name = u'Raça'
        verbose_name_plural = u'Raças'
        domain = u'Cadastros', 'fa-th'

    def __unicode__(self):
        return u'%s/%s' % (self.nome, self.especie)


class Animal(models.Model):
    SEXO_MASCULINO = 'M'
    SEXO_FEMININO = 'F'
    SEXO_CHOICES = (
        (SEXO_MASCULINO, 'Macho'),
        (SEXO_FEMININO, 'Fêmea'),
    )

    nome = models.CharField(verbose_name=u'Nome', search=True)
    sexo = models.CharField(verbose_name=u'Sexo', max_length=1, choices=SEXO_CHOICES, null=True, blank=True, search=True)
    castrado = models.NullBooleanField(verbose_name=u'Castrado', search=True)
    vacinado = models.NullBooleanField(verbose_name=u'Vacinado', search=True)
    raca = models.ForeignKey('Raca', verbose_name=u'Raça', search=True)
    porte = models.ForeignKey('Porte', verbose_name=u'Porte', null=True, blank=True, search=True)
    nascimento = models.DateField(verbose_name=u'Nascimento', null=True, blank=True, search=True)
    comportamento = models.TextField(verbose_name=u'Comportamento', blank=True, search=True)
    observacoes = models.TextField(verbose_name=u'Observações', blank=True, search=True)
    responsavel = models.ForeignKey('PessoaFisica', verbose_name=u'Responsável', help_text='Pessoa responsável pelo animal', related_name='animais', search=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': ('nome', ('sexo', 'castrado', 'vacinado'), ('raca', 'porte'), ('nascimento', 'responsavel'), 'comportamento', 'observacoes')}),
    )

    class Meta:
        ordering = ['nome']
        verbose_name = u'Animal'
        verbose_name_plural = u'Animais'
        icon = 'fa-paw'

    def __unicode__(self):
        return self.nome

    # FIXME: Só 'adm' pode
    @action(u'Colocar para Adoção', condition='pode_colocar_para_adocao', inline=True, style='popup btn-success')
    def colocar_para_adocao(self, request):
        # import ipdb; ipdb.set_trace()
        # pass
        AnimalAdocao.objects.create(animal=self, responsavel=self.responsavel)

    def pode_colocar_para_adocao(self):
        return not AnimalAdocao.objects.filter(animal=self).exists()

    # def registrar_adocao_choices(self):
    #     adotantes = PessoaFisica.objects.exclude(pk=self.responsavel.pk)
    #     return dict(adotante=adotantes)
    #
    # def registrar_adocao_initial(self):
    #     return dict(data_adocao=datetime.date.today())


class AnimalAdocao(models.Model):
    responsavel = models.ForeignKey('PessoaFisica', help_text=u'Pessoa atualmente responsável pelo animal', related_name='animais_adocao')
    animal = models.ForeignKey('Animal')
    adocao = models.ForeignKey('Adocao', null=True, blank=True)
    aprovado = models.NullBooleanField(help_text=u'Tem que ser aprovado para publicação')
    aprovador = models.ForeignKey('PessoaFisica', help_text=u'Pessoa que aprovou a publicacao', related_name='animais_adotados', null=True, blank=True)
    data = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = u'Animal para adoção'
        verbose_name_plural = u'Animais para adoção'

    def __unicode__(self):
        return u'Animal %s, responsável %s, adoção %s' % (self.animal, self.responsavel, self.adocao)


class Adocao(models.Model):
    responsavel = models.ForeignKey('PessoaFisica', help_text=u'Pessoa que aprovou a publicacao', related_name='adocoes_intermediadas', null=True, blank=True)
    adotante = models.ForeignKey('PessoaFisica', related_name='adocoes_feitas', null=True, blank=True)
    data = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = u'Adoção'
        verbose_name_plural = u'Adoções'