# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from djangoplus.admin.views import index

urlpatterns = [
    url(r'^$', index),
    url(r'^admin/$', index),
    url(r'', include('djangoplus.admin.urls')),
]


